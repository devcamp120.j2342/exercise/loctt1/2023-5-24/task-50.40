import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class Task5040 {
    public static void main(String[] args) throws Exception {
        int x1 = 4;
        int y1 = 7;
        int[] result1 = createArray(x1, y1);
        System.out.println(Arrays.toString(result1));

        int x2 = -4;
        int y2 = 7;
        int[] result2 = createArray(x2, y2);
        System.out.println(Arrays.toString(result2));

        System.out.println("--------------------------------------");

        int[] mang1 = {1, 2, 3};
        int[] mang2 = {100, 2, 1, 10};

        int[] ketQua = gopMang(mang1, mang2);
        for (int num : ketQua) {
            System.out.print(num + " ");
        }

        System.out.println("--------------------------------------");
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {100, 2, 1, 10};

        int[] result = getUniqueElements(arr1, arr2);
        System.out.print("Output: ");
        for (int num : result) {
            System.out.print(num + " ");
        }

        System.out.println("--------------------------------------");
        int[] arr = {1, 3, 1, 4, 2, 5, 6};
        sortDescending(arr);
        System.out.println(Arrays.toString(arr));

        System.out.println("--------------------------------------");
        int[] array1 = {10, 20, 30, 40, 50};
        int[] array2 = {10, 20, 30, 40, 50};
        int c1 = 0, d1 = 2;
        int c2 = 1, d2 = 2;

        // Gọi method để đổi phần tử từ vị trí x sang vị trí y cho mỗi mảng
        int[] ketQua1= swapElements(array1, c1, d1);
        int[] ketQua2 = swapElements(array2, c2, d2);

        // In kết quả
        System.out.println("Output 1: " + Arrays.toString(ketQua1));
        System.out.println("Output 2: " + Arrays.toString(ketQua2));

        System.out.println("--------------------------------------");
        int[] array = {1, 2, 1, 4, 5, 1, 1, 3, 1};
        int n = 1;

        // Gọi phương thức countOccurrences và lưu kết quả vào biến count
        int count = countOccurrences(array, n);

        // In ra số lần xuất hiện của phần tử n
        System.out.println(count);

        System.out.println("--------------------------------------");
        int [] mang = {1, 2, 3, 4, 5, 6};
        int sum = sum(mang);
        System.out.println("Tổng của mảng là: " + sum);

        System.out.println("--------------------------------------");
        int[] input = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] output = getEvenNumbers(input);
        System.out.println(Arrays.toString(output));
    }

    //Tạo một mảng gồm các phần tử có giá trị từ x đến y
    public static int[] createArray(int x, int y) {
        int length = Math.abs(y - x) + 1;
        int[] array = new int[length];
        int start = Math.min(x, y);

        for (int i = 0; i < length; i++) {
            array[i] = start + i;
        }

        return array;
    }


    //Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng nhau
    public static int[] gopMang(int[] mang1, int[] mang2) {
        Set<Integer> cacPhanTuDuyNhat = new HashSet<>();
        List<Integer> danhSachDaGop = new ArrayList<>();

        // Thêm các phần tử từ mang1 vào danhSachDaGop
        for (int num : mang1) {
            danhSachDaGop.add(num);
            cacPhanTuDuyNhat.add(num);
        }

        // Thêm các phần tử từ mang2 vào danhSachDaGop nếu chúng chưa có trong danhSachDaGop
        for (int num : mang2) {
            if (!cacPhanTuDuyNhat.contains(num)) {
                danhSachDaGop.add(num);
                cacPhanTuDuyNhat.add(num);
            }
        }

        // Chuyển danhSachDaGop thành mảng kết quả
        int[] ketQua = new int[danhSachDaGop.size()];
        for (int i = 0; i < danhSachDaGop.size(); i++) {
            ketQua[i] = danhSachDaGop.get(i);
        }

        return ketQua;
    }

    //Lấy những phần tử có giá trị không trùng nhau của 2 mảng cho trước 
    public static int[] getUniqueElements(int[] arr1, int[] arr2) {
        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();

        for (int num : arr1) {
            set1.add(num);
        }

        for (int num : arr2) {
            set2.add(num);
        }

        set1.retainAll(set2); // Lấy phần tử chung của cả hai tập hợp

        ArrayList<Integer> resultList = new ArrayList<>();

        for (int num : arr1) {
            if (!set1.contains(num)) {
                resultList.add(num);
            }
        }

        for (int num : arr2) {
            if (!set1.contains(num)) {
                resultList.add(num);
            }
        }

        int[] resultArray = new int[resultList.size()];
        for (int i = 0; i < resultList.size(); i++) {
            resultArray[i] = resultList.get(i);
        }

        return resultArray;
    }

    //Sắp xếp các phần tử theo giá trị giảm dần
    
    public static void sortDescending(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int key = arr[i];
            int j = i - 1;
            
            while (j >= 0 && arr[j] < key) {
                arr[j + 1] = arr[j];
                j--;
            }
            
            arr[j + 1] = key;
        }
    }

    //Đổi phần tử từ vị trí thứ x sang vị trí thứ y
    public static int[] swapElements(int[] array, int x, int y) {
        // Kiểm tra xem x và y có hợp lệ hay không
        if (x < 0 || x >= array.length || y < 0 || y >= array.length) {
            throw new IllegalArgumentException("Invalid indices");
        }

        // Đổi chỗ phần tử từ vị trí x sang vị trí y
        int temp = array[x];
        array[x] = array[y];
        array[y] = temp;

        return array;
    }

    //Đếm số lần xuất hiện của phần tử n trong mảng
    public static int countOccurrences(int[] array, int n) {
        // Khởi tạo biến count để đếm số lần xuất hiện
        int count = 0;

        // Duyệt qua từng phần tử trong mảng
        for (int i = 0; i < array.length; i++) {
            // Kiểm tra nếu phần tử đang xét bằng n, tăng biến count lên 1
            if (array[i] == n) {
                count++;
            }
        }

        // Trả về số lần xuất hiện của phần tử n trong mảng
        return count;
    }

    //Tính tổng các phần tử trong mảng
    public static int sum(int[] array){
        int sum = 0;
        for(int i = 0; i < array.length; i++){
            sum += array[i];
        }
        return sum;
    }

    //Từ một mảng cho trước, tạo một mảng gồm toàn các số chẵn
    public static int[] getEvenNumbers(int[] input) {
        int count = 0;
      
        // Đếm số lượng số chẵn trong mảng ban đầu
        for (int num : input) {
            if (num % 2 == 0) {
                count++;
            }
        }
      
        // Khởi tạo mảng output với kích thước bằng số lượng số chẵn
        int[] output = new int[count];
      
        // Lọc các số chẵn từ mảng ban đầu và lưu vào mảng output
        int index = 0;
        for (int num : input) {
            if (num % 2 == 0) {
                output[index] = num;
                index++;
            }
        }
      
        return output;
    }

    
}
